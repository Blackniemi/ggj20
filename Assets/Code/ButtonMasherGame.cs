﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonMasherGame : MonoBehaviour
{
    DJScript djScript;
    PlayerScript playerScript;
    Transform playerTrans;

    RectTransform repairWindowTrans;
    TMP_Text repairCountText;
    Vector3 gameOrigin;

    GameObject buttonMashFrame1;
    GameObject buttonMashFrame2;

    int repair_goal;
    int input_num;

    bool running;
    bool pushFrame;

    Vector3 playerInitPos;

    // Start is called before the first frame update
    void Start()
    {
        djScript = GameObject.Find("DJ").GetComponent<DJScript>();
        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
        playerTrans = GameObject.Find("Player").GetComponent<Transform>();
        repairWindowTrans = GameObject.Find("RepairWindow").GetComponent<RectTransform>();
        repairCountText = GameObject.Find("RepairCount").GetComponent<TMP_Text>();

        buttonMashFrame1 = GameObject.Find("ButtonMashFrame1");
        buttonMashFrame2 = GameObject.Find("ButtonMashFrame2");
        running = false;
        pushFrame = false;

        playerInitPos = playerTrans.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!running)
            return;

        if (Input.GetKeyDown(KeyCode.A) || Input.GetButtonDown("Fire1"))
        {
            input_num++;
            repairCountText.text = (repair_goal - input_num).ToString();
            playerScript.Pose("hammering2");
            djScript.Play(djScript.s_hammer_raw);
        }

        if (Input.GetKeyUp(KeyCode.A) || Input.GetButtonUp("Fire1"))
        {
            pushFrame = false;
            buttonMashFrame2.SetActive(false);
            playerScript.Pose("hammering");
        }

        if (Input.GetKey(KeyCode.A) || Input.GetButton("Fire1"))
        {
            if (pushFrame)
                return;

            pushFrame = true;
            buttonMashFrame2.SetActive(true);
        }
    }

    public void RunGame(Vector3 _gameOrigin, int _repair_goal, float x_offset)
    {
        buttonMashFrame1.SetActive(true);
        buttonMashFrame2.SetActive(true);
        gameOrigin = _gameOrigin;
        repairWindowTrans.localPosition = gameOrigin;
        repair_goal = _repair_goal;
        input_num = 0;
        repairCountText.text = repair_goal.ToString();
        running = true;

        playerTrans.position = playerInitPos + Vector3.right * x_offset;
    }

    public float Score()
    {
        running = false;

        float div = 1f - (Mathf.Abs((float)repair_goal - (float)input_num) / (float)repair_goal);

        return div;
    }

    public void Clear()
    {
        buttonMashFrame1.SetActive(false);
        buttonMashFrame2.SetActive(false);
    }
}
