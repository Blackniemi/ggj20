﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorScript : MonoBehaviour
{

    bool cursorActive;
    SpriteRenderer rend;

    public float moveSpeed;

    float cursorNormalSize = 1f;
    float cursorFocusSize = 1.6f;

    string focusPoint;
    Transform iconTrans;

    // Start is called before the first frame update
    void Start()
    {
        cursorActive = false;
        rend = GameObject.Find("icon").GetComponent<SpriteRenderer>();
        iconTrans = GameObject.Find("icon").GetComponent<Transform>();

        moveSpeed = 4f;

        Toggle(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!cursorActive)
            return;

        float x_delta = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        float y_delta = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        transform.position += new Vector3(x_delta, y_delta, 0f);
    }

    public void Toggle(bool cursorOn)
    {
        cursorActive = cursorOn;

        if (cursorActive)
        {
            rend.enabled = true;
        }
        else
        {
            rend.enabled = false;
            iconTrans.localScale = Vector3.one * cursorNormalSize;
            focusPoint = "";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("RepairPoint"))
        {
            iconTrans.localScale = Vector3.one * cursorFocusSize;
            focusPoint = other.tag;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Contains("RepairPoint"))
        {
            iconTrans.localScale = Vector3.one * cursorNormalSize;
            focusPoint = "";
        }
    }

    public void ClearFocus()
    {
        focusPoint = "";
    }

    public string CheckFocus()
    {
        return focusPoint;
    }
}
