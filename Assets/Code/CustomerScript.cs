﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CustomerScript : MonoBehaviour
{
    GameObject case1;
    GameObject case2;
    GameObject case3;
    GameObject case1_best;
    GameObject case2_best;
    GameObject case3_best;
    GameObject case3_neutral;
    GameObject case3_bad;
    GameObject case1_worst;
    GameObject case2_worst;

    GameObject customerBubble;
    TMP_Text customerLine;

    private void Awake()
    {
        case1 = GameObject.Find("dialogi_client1_sad");
        case2 = GameObject.Find("dialogi_client2_sad");
        case3 = GameObject.Find("dialogi_client3_sad");
        case1_best = GameObject.Find("dialogi_client1_best");
        case2_best = GameObject.Find("dialogi_client2_best");
        case3_best = GameObject.Find("dialogi_client3_best");
        case3_neutral = GameObject.Find("dialogi_client3_neutral");
        case3_bad = GameObject.Find("dialogi_client3_bad");
        case1_worst = GameObject.Find("dialogi_client1_worst");
        case2_worst = GameObject.Find("dialogi_client2_worst");

        customerBubble = GameObject.Find("AsiakasBubble");
        customerLine = GameObject.Find("AsiakasText").GetComponent<TMP_Text>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Pose(string pose)
    {
        NoPose();

        switch (pose)
        {
            case "case1":
                case1.SetActive(true);
                customerBubble.SetActive(true);
                break;
            case "case2":
                case2.SetActive(true);
                customerBubble.SetActive(true);
                break;
            case "case3":
                case3.SetActive(true);
                customerBubble.SetActive(true);
                break;

            case "case1_worst":
                case1_worst.SetActive(true);
                customerBubble.SetActive(true);
                break;
            case "case1_best":
                case1_best.SetActive(true);
                customerBubble.SetActive(true);
                break;

            case "case2_worst":
                case2_worst.SetActive(true);
                customerBubble.SetActive(true);
                break;
            case "case2_best":
                case2_best.SetActive(true);
                customerBubble.SetActive(true);
                break;

            case "case3_bad":
                case3_bad.SetActive(true);
                customerBubble.SetActive(true);
                break;
            case "case3_neutral":
                case3_neutral.SetActive(true);
                customerBubble.SetActive(true);
                break;
            case "case3_best":
                case3_best.SetActive(true);
                customerBubble.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void NoPose()
    {
        case1.SetActive(false);
        case2.SetActive(false);
        case3.SetActive(false);
        case1_best.SetActive(false);
        case2_best.SetActive(false);
        case3_best.SetActive(false);
        case3_neutral.SetActive(false);
        case3_bad.SetActive(false);
        case1_worst.SetActive(false);
        case2_worst.SetActive(false);

        customerBubble.SetActive(false);
    }
}
