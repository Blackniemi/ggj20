﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DJScript : MonoBehaviour
{
    public AudioSource sp;
    AudioSource sp_action;

    public AudioClip[] s_actions = new AudioClip[5];

    public AudioClip s_case_intro1;
    public AudioClip s_case_intro2;
    public AudioClip s_case1_intro1;
    public AudioClip s_case1_intro2;
    public AudioClip s_case2_intro;
    public AudioClip s_case3_intro;

    public AudioClip s_case2_perfect;

    public AudioClip s_best_effort;
    public AudioClip s_good_effort;
    public AudioClip s_poor_effort1;
    public AudioClip s_poor_effort2;
    public AudioClip s_zero_effort1;
    public AudioClip s_zero_effort2;
    public AudioClip s_zero_effort3;

    public AudioClip s_drill_action;
    public AudioClip s_pora;

    public AudioClip s_hammer_action;
    public AudioClip s_hammer_raw;

    public AudioClip s_minigame_fail1;
    public AudioClip s_minigame_fail2;
    public AudioClip s_minigame_success1;
    public AudioClip s_minigame_success2;

    public AudioClip s_slow1;
    public AudioClip s_slow2;
    public AudioClip s_slow3;

    // Start is called before the first frame update
    void Start()
    {
        s_actions[0] = (AudioClip) Resources.Load("SFX/action");
        s_actions[1] = (AudioClip)Resources.Load("SFX/action2");
        s_actions[2] = (AudioClip)Resources.Load("SFX/action3");
        s_actions[3] = (AudioClip)Resources.Load("SFX/action4");
        s_actions[4] = (AudioClip)Resources.Load("SFX/action5");

        s_case_intro1 = (AudioClip)Resources.Load("SFX/caseintro");
        s_case_intro2 = (AudioClip)Resources.Load("SFX/caseintro2");
        s_case1_intro1 = (AudioClip)Resources.Load("SFX/case1intro");
        s_case1_intro2 = (AudioClip)Resources.Load("SFX/case1intro2");
        s_case2_intro = (AudioClip)Resources.Load("SFX/case2intro");
        s_case3_intro = (AudioClip)Resources.Load("SFX/case3intro");

        s_case2_perfect = (AudioClip)Resources.Load("SFX/case2perfect");

        s_best_effort = (AudioClip)Resources.Load("SFX/best_effort");
        s_good_effort = (AudioClip)Resources.Load("SFX/good_effort");
        s_poor_effort1 = (AudioClip)Resources.Load("SFX/poor_effort");
        s_poor_effort2 = (AudioClip)Resources.Load("SFX/poor_effort2");
        s_zero_effort1 = (AudioClip)Resources.Load("SFX/zero_effort");
        s_zero_effort2 = (AudioClip)Resources.Load("SFX/zero_effort2");
        s_zero_effort3 = (AudioClip)Resources.Load("SFX/zero_effort3");

        s_drill_action = (AudioClip)Resources.Load("SFX/drill_action");
        s_pora = (AudioClip)Resources.Load("SFX/pora");

        s_hammer_action = (AudioClip)Resources.Load("SFX/hammer_action");
        s_hammer_raw = (AudioClip)Resources.Load("SFX/hammer_raw");

        s_minigame_fail1 = (AudioClip)Resources.Load("SFX/minigame_fail");
        s_minigame_fail2 = (AudioClip)Resources.Load("SFX/minigame_fail2");
        s_minigame_success1 = (AudioClip)Resources.Load("SFX/minigame_success");
        s_minigame_success2 = (AudioClip)Resources.Load("SFX/minigame_success2");

        s_slow1 = (AudioClip)Resources.Load("SFX/slow");
        s_slow2 = (AudioClip)Resources.Load("SFX/slow2");
        s_slow3 = (AudioClip)Resources.Load("SFX/slow3");

        sp = gameObject.GetComponent<AudioSource>();
        sp_action = GameObject.Find("MASTER").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play(AudioClip clip)
    {
        sp.PlayOneShot(clip, 1f);
    }

    public void PlayAction()
    {
        int rand = Random.Range(0, 5);
        sp_action.PlayOneShot(s_actions[rand]);
    }
}
