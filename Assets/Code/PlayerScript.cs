﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerScript : MonoBehaviour
{
    GameObject gIdle;
    GameObject gHammer1;
    GameObject gHammer2;
    GameObject gHammerFrontHand;
    GameObject gDrilling;
    GameObject gCase;
    GameObject gHappy;
    GameObject gFukoff;
    GameObject gThumbsUp;
    GameObject gWtf;

    GameObject playerBubble;
    TMP_Text playerLine;

    private void Awake()
    {
        gIdle = GameObject.Find("riksa_Idling");
        gHammer1 = GameObject.Find("hammer_frame1");
        gHammer2 = GameObject.Find("hammer_frame2");
        gHammerFrontHand = GameObject.Find("hammer_handToFront");
        gDrilling = GameObject.Find("drilling");
        gCase = GameObject.Find("dialogi_Jaska_neutral");
        gHappy = GameObject.Find("dialogi_Jaska_happy");
        gFukoff = GameObject.Find("dialogi_Jaska_fukoff");
        gThumbsUp = GameObject.Find("dialogi_Jaska_thumbsup");
        gWtf = GameObject.Find("dialogi_Jaska_wtf");

        playerBubble = GameObject.Find("JaskaBubble");
        playerLine = GameObject.Find("JaskaText").GetComponent<TMP_Text>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Pose(string pose)
    {
        NoPose();

        switch(pose)
        {
            case "idle":
                gIdle.SetActive(true);
                break;

            case "case1":
                gCase.SetActive(true);
                playerBubble.SetActive(true);
                break;

            case "case2":
                gCase.SetActive(true);
                playerBubble.SetActive(true);
                break;

            case "case3":
                gCase.SetActive(true);
                playerBubble.SetActive(true);
                break;

            case "happy":
                gHappy.SetActive(true);
                playerBubble.SetActive(true);
                break;

            case "fukoff":
                gFukoff.SetActive(true);
                playerBubble.SetActive(true);
                break;

            case "thumbsup":
                gThumbsUp.SetActive(true);
                playerBubble.SetActive(true);
                break;

            case "wtf":
                gWtf.SetActive(true);
                playerBubble.SetActive(true);
                break;

            case "hammering":
                gHammer1.SetActive(true);
                break;

            case "hammering2":
                gHammer2.SetActive(true);
                break;

            case "drilling":
                gDrilling.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void NoPose()
    {
        gIdle.SetActive(false);
        gHammer1.SetActive(false);
        gHammer2.SetActive(false);
        gHammerFrontHand.SetActive(false);
        gDrilling.SetActive(false);
        gCase.SetActive(false);
        gHappy.SetActive(false);
        gFukoff.SetActive(false);
        gThumbsUp.SetActive(false);
        gWtf.SetActive(false);

        playerBubble.SetActive(false);
    }
}
