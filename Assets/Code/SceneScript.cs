﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SceneScript : MonoBehaviour
{
    DJScript djScript;

    public enum Result
    {
        NO_EFFORT,
        BAD,
        GOOD,
        BEST
    };

    PlayerScript playerScript;
    GameObject playerBubble;
    TMP_Text playerLine;
    CustomerScript customerScript;
    GameObject customerBubble;
    TMP_Text customerLine;

    ButtonMasherGame buttonMasherGame;
    SpedeGame spedeGame;
    SpinGame spinGame;
    MeterGame meterGame;

    RectTransform repairWindowTrans;

    TMP_Text subs;

    GameObject titleImage;

    CursorScript cursorScript;

    GameObject Car;
    GameObject Jar;
    GameObject Human;

    Vector3 offScreenPos = new Vector3(10000, 10000, 0);


    // Start is called before the first frame update
    void Start()
    {
        djScript = GameObject.Find("DJ").GetComponent<DJScript>();
        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
        playerBubble = GameObject.Find("JaskaBubble");
        playerLine = GameObject.Find("JaskaText").GetComponent<TMP_Text>();
        customerScript = GameObject.Find("Customer").GetComponent<CustomerScript>();
        customerBubble = GameObject.Find("AsiakasBubble");
        customerLine = GameObject.Find("AsiakasText").GetComponent<TMP_Text>();
        buttonMasherGame = GameObject.Find("ButtonRepair").GetComponent<ButtonMasherGame>();
        spedeGame = GameObject.Find("SpedeRepair").GetComponent<SpedeGame>();
        spinGame = GameObject.Find("SpinRepair").GetComponent<SpinGame>();
        meterGame = GameObject.Find("MeterRepair").GetComponent<MeterGame>();

        subs = GameObject.Find("Subtitle").GetComponent<TMP_Text>();

        repairWindowTrans = GameObject.Find("RepairWindow").GetComponent<RectTransform>();

        titleImage = GameObject.Find("Title");

        cursorScript = GameObject.Find("Cursor").GetComponent<CursorScript>();

        Car = GameObject.Find("auto_base");
        Jar = GameObject.Find("ruukku_base");
        Human = GameObject.Find("heppu_base");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleTitle(bool enable)
    {
        ClearAll();

        if (enable)
        {
            titleImage.SetActive(true);
        }
        else
        {
            titleImage.SetActive(false);
        }
    }

    public void ToggleCase(bool enable, int case_id)
    {
        ClearAll();

        AudioClip caseClip = djScript.s_case_intro1;

        int rand = Random.Range(1, 101);

        if (enable)
        {
            //SetSubs("Press A to begin");

            switch (case_id)
            {
                case 1:
                    playerScript.Pose("case1");
                    playerLine.text = "Lemme see.";
                    customerScript.Pose("case1");
                    customerLine.text = "I got this hi-tech car all the big boys have been talking about for really cheap, but the bloody thing went bust during the very first drive to the grocery store! Can you fix it?";
                    if (rand < 35)
                        caseClip = djScript.s_case1_intro1;
                    else
                        caseClip = djScript.s_case1_intro2;
                    break;

                case 2:
                    playerScript.Pose("case2");
                    playerLine.text = "Aren't we demanding.";
                    customerScript.Pose("case2");
                    customerLine.text = "I bought this antique vase from that handsome auction hunk from TV, but my poodle managed to knock it off my hands! I demand you to fix it!";
                    caseClip = djScript.s_case2_intro;
                    break;

                case 3:
                    playerScript.Pose("case3");
                    playerLine.text = "...Well, this is new.";
                    customerScript.Pose("case3");
                    customerLine.text = "Everything is awful! My work sucks, my life sucks, my vacuum blows! I'm so bad at everything! PLEASE fix my self-esteem!";
                    caseClip = djScript.s_case3_intro;
                    break;

                default:
                    break;
            }
        }
        else
        {
            SetSubs("");
        }

        // Chance that Jaska takes the vocal stage
        if (rand > 70)
            caseClip = djScript.s_case_intro1;
        if (rand > 85)
            caseClip = djScript.s_case_intro2;

        djScript.Play(caseClip);
    }

    public void ToggleGame(bool enable, int case_id)
    {
        ClearAll();

        if (enable)
        {
            cursorScript.Toggle(true);
            playerScript.Pose("idle");

            switch (case_id)
            {
                case 1:
                    Car.SetActive(true);
                    break;

                case 2:
                    Jar.SetActive(true);
                    break;

                case 3:
                    Human.SetActive(true);
                    break;

                default:
                    break;
            }
        }
        else
        {
            cursorScript.Toggle(false);
        }
    }

    public void ToggleResults(bool enable, int case_id, Result result)
    {
        ClearAll();
        //SetSubs("Press A to continue");

        playerBubble.SetActive(true);
        customerBubble.SetActive(true);

        AudioClip resultClip = djScript.s_zero_effort1;

        int rand = Random.Range(1, 101);

        switch (result)
        {
            case Result.NO_EFFORT:
                if (rand > 33)
                    resultClip = djScript.s_zero_effort2;
                if (rand > 66)
                    resultClip = djScript.s_zero_effort3;
                break;

            case Result.BAD:
                resultClip = djScript.s_poor_effort1;
                if (rand > 50)
                    resultClip = djScript.s_poor_effort2;
                break;

            case Result.GOOD:
                resultClip = djScript.s_good_effort;
                break;

            case Result.BEST:
                resultClip = djScript.s_best_effort;
                break;
        }

        switch (case_id)
        {
            case 1:
                switch (result)
                {
                    case Result.NO_EFFORT:
                        playerScript.Pose("fukoff");
                        playerLine.text = "Look, man, you bought a bootleg trend car, you deserve this.";
                        customerScript.Pose("case1_worst");
                        customerLine.text = "You didn't even touch it! What the hell, man? I'm a paying customer!";
                        break;

                    case Result.BAD:
                        playerScript.Pose("case1");
                        playerLine.text = "Not my fault you crashed it this bad.";
                        customerScript.Pose("case1_worst");
                        customerLine.text = "Did you even fix anything? It still looks like shit!I'm gonna be the laughing stock of my workplace!";
                        break;

                    case Result.GOOD:
                        playerScript.Pose("case1");
                        playerLine.text = "Glad to help.";
                        customerScript.Pose("case1_best");
                        customerLine.text = "Whew, now it looks somewhat decent. Now my reputation won't be completely dragged through the mud, at least. Thanks.";
                        break;

                    case Result.BEST:
                        playerScript.Pose("thumbsup");
                        playerLine.text = "Nah, I'm a carsaver.";
                        customerScript.Pose("case1_best");
                        customerLine.text = "Holy crackers! It looks perfect! My co-workers are gonna be SO jealous! You're a lifesaver!";
                        break;
                }
                break;

            case 2:
                switch (result)
                {
                    case Result.NO_EFFORT:
                        playerScript.Pose("fukoff");
                        playerLine.text = "Don't have one. Train your mutt better, lady.";
                        customerScript.Pose("case2_worst");
                        customerLine.text = "You barely even looked at it! This is an outrage, your manager will hear from this!";
                        break;

                    case Result.BAD:
                        playerScript.Pose("case1");
                        playerLine.text = "Look here, lady, sometimes you can only do so much.";
                        customerScript.Pose("case2_worst");
                        customerLine.text = "This looks awful! I can't put this on display, what will my neighbours say?";
                        break;

                    case Result.GOOD:
                        playerScript.Pose("case1");
                        playerLine.text = "No biggie.";
                        customerScript.Pose("case2_best");
                        customerLine.text = "Well, it's fine. Most of the damage seems to be undone. You saved me some hassle, so thanks for that, dear.";
                        break;

                    case Result.BEST:
                        playerScript.Pose("thumbsup");
                        playerLine.text = "I'll take buns, if you will. Goes better with coffee.";
                        customerScript.Pose("case2_best");
                        customerLine.text = "Oh my, it's like it never broke at all! Thank you so much, dear, I'll bring you some cookies the next time I'm here!";
                        resultClip = djScript.s_case2_perfect;
                        break;
                }
                break;

            case 3:
                switch (result)
                {
                    case Result.NO_EFFORT:
                        playerScript.Pose("fukoff");
                        playerLine.text = "As much as I'd like to, I can't. This kind of stuff is on you.";
                        customerScript.Pose("case3_bad");
                        customerLine.text = "Oh, I get it. Even you don't care. It's okay, I'm used to being ignored. I knew you couldn't help me.";
                        break;

                    case Result.BAD:
                        playerScript.Pose("case1");
                        playerLine.text = "Kinda want to be careful with the hammer and drill on you, man.";
                        customerScript.Pose("case3_bad");
                        customerLine.text = "I mean I guess this is a start, but... I still feel kind of wrong. Did you fix me hard enough?";
                        break;

                    case Result.GOOD:
                        playerScript.Pose("thumbsup");
                        playerLine.text = "Good for you, man. Go and rock the world.";
                        customerScript.Pose("case3_neutral");
                        customerLine.text = "Wow, I'm feeling better already! This is a good start for a new, better direction for me. Thanks a bunch, chum!";
                        break;

                    case Result.BEST:
                        playerScript.Pose("wtf");
                        playerLine.text = "(...My hammer can do this?) ";
                        customerScript.Pose("case3_best");
                        customerLine.text = "I've been reborn! I've found the shining light of my life, and I'M THE SUN. Thank you, my good fellow, you've saved my life!";
                        break;
                }
                break;
        }

        djScript.Play(resultClip);
    }

    public void SetSubs(string text)
    {
        subs.text = text;
    }

    public void ClearAll()
    {
        playerScript.Pose("");
        customerScript.Pose("");
        SetSubs("");
        repairWindowTrans.position = offScreenPos;
        titleImage.SetActive(false);
        Car.SetActive(false);
        Jar.SetActive(false);
        Human.SetActive(false);
        cursorScript.ClearFocus();
        // NO TOGGLE* CALLS HERE
    }

    public void ClearMiniGames()
    {
        buttonMasherGame.Clear();
        spinGame.Clear();
        spedeGame.Clear();
    }

    public void LaunchMiniGame(int case_id, int rep_id)
    {
        djScript.PlayAction();

        switch(case_id)
        {
            case 1:
                LaunchCarRepair(rep_id);
                break;

            case 2:
                LaunchJarRepair(rep_id);
                break;

            case 3:
                LaunchMindRepair(rep_id);
                break;
        }

        cursorScript.Toggle(false);
    }

    void LaunchCarRepair(int rep_id)
    {
        switch (rep_id)
        {
            case 1:
                playerScript.Pose("hammering");
                buttonMasherGame.RunGame(new Vector3(120f, -30f, 0f), 20, 0f);
                Destroy(GameObject.Find("CarRep1"));
                break;

            case 2:
                break;

            case 3:
                playerScript.Pose("hammering");
                spedeGame.RunGame(new Vector3(80f, -150f, 0f), 6, 7f);
                Destroy(GameObject.Find("CarRep3"));
                break;

            case 4:
                playerScript.Pose("drilling");
                spinGame.RunGame(new Vector3(0f, -180f, 0f), 6, -1f);
                Destroy(GameObject.Find("CarRep4"));
                break;
        }
    }

    void LaunchJarRepair(int rep_id)
    {
        switch (rep_id)
        {
            case 1:
                playerScript.Pose("hammering");
                buttonMasherGame.RunGame(new Vector3(200f, -60f, 0f), 30, 2f);
                Destroy(GameObject.Find("JarRep1"));
                break;

            case 2:
                playerScript.Pose("drilling");
                spinGame.RunGame(new Vector3(100f, -180f, 0f), 9, 1f);
                Destroy(GameObject.Find("JarRep4"));
                break;

            case 3:
                playerScript.Pose("hammering");
                spedeGame.RunGame(new Vector3(80f, -150f, 0f), 8, 5.5f);
                Destroy(GameObject.Find("JarRep3"));
                break;

            case 4:

                break;
        }
    }

    void LaunchMindRepair(int rep_id)
    {
        switch (rep_id)
        {
            case 1: // Face
                break;

            case 2: // Pants
                playerScript.Pose("drilling");
                spinGame.RunGame(new Vector3(100f, -180f, 0f), 12, 2.5f);
                Destroy(GameObject.Find("HumanRep2"));
                break;

            case 3: // Hair
                playerScript.Pose("hammering");
                buttonMasherGame.RunGame(new Vector3(200f, -60f, 0f), 35, 3.5f);
                Destroy(GameObject.Find("HumanRep3"));
                break;

            case 4: // Torso
                playerScript.Pose("hammering");
                spedeGame.RunGame(new Vector3(80f, -150f, 0f), 10, 3.5f);
                Destroy(GameObject.Find("HumanRep4"));
                break;
        }
    }
}
