﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SpedeGame : MonoBehaviour
{
    DJScript djScript;
    PlayerScript playerScript;
    Transform playerTrans;
    RectTransform repairWindowTrans;
    RectTransform repairWindowBubbleTrans;
    TMP_Text repairCountText;
    Vector3 gameOrigin;

    GameObject buttonMashFrame1;
    GameObject buttonMashFrame2;

    GameObject gLeft;
    GameObject gUp;
    GameObject gRight;
    GameObject gDown;
    GameObject gPressImage;

    int repair_goal;
    public int input_num;
    /*public enum Dir
    {
        UP,
        RIGHT,
        DOWN,
        LEFT
    };
    public Dir current_dir;*/
    public int current_dir;

    public bool running;
    float thr;

    Vector3 playerInitPos;

    // Start is called before the first frame update
    void Start()
    {
        djScript = GameObject.Find("DJ").GetComponent<DJScript>();
        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
        playerTrans = GameObject.Find("Player").GetComponent<Transform>();
        repairWindowTrans = GameObject.Find("RepairWindow").GetComponent<RectTransform>();
        repairWindowBubbleTrans = GameObject.Find("RepairWindowBubble").GetComponent<RectTransform>();
        repairCountText = GameObject.Find("RepairCount").GetComponent<TMP_Text>();

        buttonMashFrame1 = GameObject.Find("ButtonMashFrame1");
        buttonMashFrame2 = GameObject.Find("ButtonMashFrame2");

        gLeft = GameObject.Find("Left");
        gUp = GameObject.Find("Up");
        gRight = GameObject.Find("Right");
        gDown = GameObject.Find("Down");
        gPressImage = GameObject.Find("PressImage");

        running = false;
        thr = 0.05f;
        playerInitPos = playerTrans.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!running)
            return;

        float x_input = Input.GetAxis("Horizontal");
        float y_input = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.LeftArrow) ||
            Input.GetKey(KeyCode.RightArrow) ||
            Input.GetKey(KeyCode.UpArrow) ||
            Input.GetKey(KeyCode.DownArrow))
        {
        }
        else
        {
            buttonMashFrame2.SetActive(false);
            playerScript.Pose("hammering");
        }

        switch (current_dir)
        {
            case 1: // LEFT

                if (x_input <= -thr)
                {
                    ArrowDone();
                }

                break;

            case 2: // UP

                if (y_input >= thr)
                {
                    ArrowDone();
                }

                break;

            case 3: // RIGHT

                if (x_input >= thr)
                {
                    ArrowDone();
                }

                break;

            case 4: // DOWN

                if (y_input <= -thr)
                {
                    ArrowDone();
                }

                break;
        }
    }

    void ArrowDone()
    {
        input_num++;
        repairCountText.text = (repair_goal - input_num).ToString();

        playerScript.Pose("hammering2");
        djScript.Play(djScript.s_hammer_raw);

        int rand;

        do
        {
            rand = Random.Range(1, 5);
        } while (rand == current_dir);

        current_dir = rand;

        gLeft.SetActive(false);
        gUp.SetActive(false);
        gRight.SetActive(false);
        gDown.SetActive(false);

        switch (current_dir)
        {
            case 1:
                gLeft.SetActive(true);
                break;

            case 2:
                gUp.SetActive(true);
                break;

            case 3:
                gRight.SetActive(true);
                break;

            case 4:
                gDown.SetActive(true);
                break;
        }
    }

    public void RunGame(Vector3 _gameOrigin, int _repair_goal, float x_offset)
    {
        buttonMashFrame1.SetActive(true);
        buttonMashFrame2.SetActive(true);
        gameOrigin = _gameOrigin;
        repairWindowTrans.localPosition = gameOrigin;
        repairWindowBubbleTrans.localRotation = Quaternion.Euler(0f, 180f, 0f);
        repair_goal = _repair_goal;
        input_num = 0;
        repairCountText.text = repair_goal.ToString();
        gPressImage.SetActive(true);
        running = true;
        current_dir = 1;
        gLeft.SetActive(true);

        playerTrans.position = playerInitPos + Vector3.right * x_offset;
    }

    public float Score()
    {
        running = false;

        float div = 1f - (Mathf.Abs((float)repair_goal - (float)input_num) / (float)repair_goal);

        return div;
    }

    public void Clear()
    {
        repairWindowBubbleTrans.localRotation = Quaternion.Euler(0f, 0f, 0f);
        playerTrans.position = playerInitPos;
        
        gPressImage.SetActive(false);
        gLeft.SetActive(false);
        gUp.SetActive(false);
        gRight.SetActive(false);
        gDown.SetActive(false);

        buttonMashFrame1.SetActive(false);
        buttonMashFrame2.SetActive(false);
        running = false;
    }
}
