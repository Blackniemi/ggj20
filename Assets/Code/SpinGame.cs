﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpinGame : MonoBehaviour
{
    DJScript djScript;
    Transform playerTrans;
    RectTransform repairWindowTrans;
    TMP_Text repairCountText;
    Vector3 gameOrigin;

    GameObject spinImage;
    Transform spinFrameTrans;

    int repair_goal;
    public int input_num;
    public enum Dir
    {
        UP,
        RIGHT,
        DOWN,
        LEFT
    };
    public Dir current_dir;

    bool running;

    float thr;
    float angle;
    float spinRate;

    Vector3 playerInitPos;

    // Start is called before the first frame update
    void Start()
    {
        djScript = GameObject.Find("DJ").GetComponent<DJScript>();
        playerTrans = GameObject.Find("Player").GetComponent<Transform>();
        repairWindowTrans = GameObject.Find("RepairWindow").GetComponent<RectTransform>();
        repairCountText = GameObject.Find("RepairCount").GetComponent<TMP_Text>();

        spinImage = GameObject.Find("SpinImage");
        spinFrameTrans = GameObject.Find("SpinFrame").GetComponent<Transform>();
        running = false;

        playerInitPos = playerTrans.position;

        thr = 0.05f;
        angle = 0f;
        spinRate = -10f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!running)
            return;

        float x_input = Input.GetAxis("Horizontal");
        float y_input = Input.GetAxis("Vertical");

        angle += Time.deltaTime * spinRate;
        Vector3 spinVector = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0f) * 20f;
        spinFrameTrans.localPosition = new Vector3(0f, 105f, 0f) + spinVector;


        switch (current_dir)
        {
            case Dir.UP:
                if (x_input >= thr)
                {
                    current_dir = Dir.RIGHT;
                }
                break;

            case Dir.RIGHT:
                if (y_input <= -thr)
                {
                    current_dir = Dir.DOWN;
                }
                break;

            case Dir.DOWN:
                if (x_input <= -thr)
                {
                    current_dir = Dir.LEFT;
                }
                break;

            case Dir.LEFT:
                if (y_input >= thr)
                {
                    current_dir = Dir.UP;
                    input_num++;
                    repairCountText.text = (repair_goal - input_num).ToString();
                }
                break;
        }
    }

    public void RunGame(Vector3 _gameOrigin, int _repair_goal, float x_offset)
    {
        gameOrigin = _gameOrigin;
        repairWindowTrans.localPosition = gameOrigin;
        repair_goal = _repair_goal;
        input_num = 0;
        repairCountText.text = repair_goal.ToString();
        spinFrameTrans.gameObject.SetActive(true);
        spinImage.SetActive(true);
        running = true;
        current_dir = Dir.LEFT;
        spinFrameTrans.localPosition = new Vector3(-25f, 105f, 0f);
        djScript.Play(djScript.s_pora);

        playerTrans.position = playerInitPos + Vector3.right * x_offset;
    }

    public float Score()
    {
        running = false;

        float div = 1f - (Mathf.Abs((float)repair_goal - (float)input_num) / (float)repair_goal);

        return div;
    }

    public void Clear()
    {
        playerTrans.position = playerInitPos;
        spinFrameTrans.gameObject.SetActive(false);
        spinImage.SetActive(false);
    }
}
