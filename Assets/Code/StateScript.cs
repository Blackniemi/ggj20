﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StateScript : MonoBehaviour
{
    DJScript djScript;

    public enum GameState
    {
        TITLE,
        CASE,
        GAME,
        RESULTS,
        ENDING
    };

    public GameState gameState;
    SceneScript sceneScript;

    CursorScript cursorScript;

    ButtonMasherGame buttonMasherGame;
    SpedeGame spedeGame;
    SpinGame spinGame;
    MeterGame meterGame;

    TMP_Text gameTimerText;
    float gameTimer;

    GameObject endingScreen;
    TMP_Text endingScoreText;

    GameObject kukkaAlive;
    GameObject heppuFaceBest;
    GameObject heppuHousuBest;
    GameObject heppuHousuNeutral;
    GameObject heppuTorsoBest;
    GameObject heppuTorsoNeutral;
    GameObject heppuTukkaBest;
    GameObject heppuTukkaNeutral;

    public int case_id;

    public bool inMiniGame;
    public int rep_current_id;
    public int repairs_done;
    public float[] case1_scores = new float[] { 0f, 0f, 0f, 0f };
    public float[] case2_scores = new float[] { 0f, 0f, 0f, 0f };
    public float[] case3_scores = new float[] { 0f, 0f, 0f, 0f };
    public float case_total_score;

    public int reps_required;

    public float idleTimer;


    // Start is called before the first frame update
    void Start()
    {
        djScript = GameObject.Find("DJ").GetComponent<DJScript>();
        sceneScript = GetComponent<SceneScript>();

        cursorScript = GameObject.Find("Cursor").GetComponent<CursorScript>();

        endingScreen = GameObject.Find("EndScreen");
        endingScoreText = GameObject.Find("EndScoreText").GetComponent<TMP_Text>();

        kukkaAlive = GameObject.Find("ruukku_kukka_alive");
        heppuFaceBest = GameObject.Find("heppu_face_best");
        heppuHousuBest = GameObject.Find("heppu_housu_best");
        heppuHousuNeutral = GameObject.Find("heppu_housu_neutral");
        heppuTorsoBest = GameObject.Find("heppu_torso_best");
        heppuTorsoNeutral = GameObject.Find("heppu_torso_neutral");
        heppuTukkaBest = GameObject.Find("heppu_tukka_best");
        heppuTukkaNeutral = GameObject.Find("heppu_tukka_neutral");

        buttonMasherGame = GameObject.Find("ButtonRepair").GetComponent<ButtonMasherGame>();
        spedeGame = GameObject.Find("SpedeRepair").GetComponent<SpedeGame>();
        spinGame = GameObject.Find("SpinRepair").GetComponent<SpinGame>();
        meterGame = GameObject.Find("MeterRepair").GetComponent<MeterGame>();

        gameTimerText = GameObject.Find("GameTimer").GetComponent<TMP_Text>();
        gameTimer = 0f;
        inMiniGame = false;
        rep_current_id = -1;
        repairs_done = 0;
        reps_required = 3;
        endingScoreText.text = "";
        case_total_score = 0f;

        SetState(GameState.TITLE);
    }

    // Update is called once per frame
    void Update()
    {
        switch(gameState)
        {
            case GameState.TITLE:

                if (Input.anyKeyDown)
                {
                    SetState(GameState.CASE);
                }

                break;

            case GameState.CASE:

                if (Input.GetKeyDown(KeyCode.A) || Input.GetButtonDown("Fire1"))
                {
                    SetState(GameState.GAME);
                }

                break;

            case GameState.GAME:

                if (inMiniGame)
                {
                    gameTimer -= Time.deltaTime;
                    int seconds = Mathf.FloorToInt(gameTimer);
                    gameTimerText.text = seconds.ToString() + ":" + Mathf.FloorToInt((gameTimer - (float)seconds) * 100).ToString();

                    if (gameTimer <= 0f)
                    {
                        gameTimerText.text = "00:00";
                        sceneScript.ClearMiniGames();
                        inMiniGame = false;
                        repairs_done++;
                        float score = AddRepairResult();

                        if (repairs_done == reps_required) // TODO: Make it 4 in the end!
                        {
                            SetState(GameState.RESULTS);
                        }
                        else
                        {
                            int rand = Random.Range(1, 3);
                            if (score < 0.5f)
                            {
                                if (rand == 1)
                                    djScript.Play(djScript.s_minigame_fail1);
                                else
                                    djScript.Play(djScript.s_minigame_fail2);
                            }
                            else
                            {
                                if (rand == 1)
                                    djScript.Play(djScript.s_minigame_success1);
                                else
                                    djScript.Play(djScript.s_minigame_success2);
                            }

                            sceneScript.ToggleGame(true, case_id);
                            idleTimer = 5f;
                        }
                    }

                    break;
                }
                else
                {
                    idleTimer -= Time.deltaTime;
                    if (Input.anyKey)
                        idleTimer = 5f;
                }

                if (Input.GetKeyDown(KeyCode.A) || Input.GetButtonDown("Fire1"))
                {
                    string repPoint = cursorScript.CheckFocus();

                    if (!repPoint.Equals(""))
                    {
                        rep_current_id = int.Parse(repPoint.Substring(repPoint.Length - 1));
                        LaunchGame(rep_current_id);
                        gameTimer = 5f;
                        return;
                    }
                }

                if (idleTimer <= 0f)
                {
                    int rand = Random.Range(0, 3);
                    AudioClip idleClip = djScript.s_slow1;
                    if (rand == 1)
                        idleClip = djScript.s_slow2;
                    else if (rand == 2)
                        idleClip = djScript.s_slow3;

                    djScript.Play(idleClip);
                    idleTimer = 5f + idleClip.length;
                }

                break;

            case GameState.RESULTS:

                if (Input.GetKeyDown(KeyCode.A) || Input.GetButtonDown("Fire1"))
                {
                    if (case_id == 4)
                        SetState(GameState.ENDING);
                    else
                        SetState(GameState.CASE);
                }

                break;
        }
    }

    void SetState(GameState newState)
    {
        switch (newState)
        {
            case GameState.TITLE:

                sceneScript.ToggleTitle(true);
                case_id = 1;
                endingScreen.SetActive(false);
                kukkaAlive.SetActive(false);
                heppuFaceBest.SetActive(false);
                heppuHousuBest.SetActive(false);
                heppuHousuNeutral.SetActive(false);
                heppuTorsoBest.SetActive(false);
                heppuTorsoNeutral.SetActive(false);
                heppuTukkaBest.SetActive(false);
                heppuTukkaNeutral.SetActive(false);

                break;

            case GameState.CASE:

                
                sceneScript.ToggleCase(true, case_id);

                break;

            case GameState.GAME:

                sceneScript.ClearMiniGames();
                sceneScript.ToggleGame(true, case_id);
                idleTimer = 5f;

                break;

            case GameState.RESULTS:

                SceneScript.Result result = SceneScript.Result.NO_EFFORT;
                float result_f = AnalyzeResult();
                if (result_f == 0f)
                    result = SceneScript.Result.NO_EFFORT;
                else if (result_f <= 0.50f)
                    result = SceneScript.Result.BAD;
                else if (result_f < 1f)
                    result = SceneScript.Result.GOOD;
                else if (result_f >= 1f)
                    result = SceneScript.Result.BEST;

                sceneScript.ToggleResults(true, case_id, result);

                case_id++;
                repairs_done = 0;

                if (case_id == 2)
                    Destroy(GameObject.Find("kyltti1"));
                if (case_id == 3)
                    Destroy(GameObject.Find("kyltti2"));

                if (case_id < 5)
                    case_total_score += result_f;

                break;

            case GameState.ENDING:
                sceneScript.ClearAll();
                endingScreen.SetActive(true);

                int totalScore = Mathf.FloorToInt(100*case_total_score / 3f);

                endingScoreText.text = totalScore.ToString() + "%";
                break;
        }

        gameState = newState;
    }

    float AnalyzeResult()
    {
        float result = 0f;

        switch (case_id)
        {
            case 1:
                result = (case1_scores[0] + case1_scores[1] + case1_scores[2] + case1_scores[3]) / (float)reps_required;
                break;

            case 2:
                result = (case2_scores[0] + case2_scores[1] + case2_scores[2] + case2_scores[3]) / (float)reps_required;
                break;

            case 3:
                result = (case3_scores[0] + case3_scores[1] + case3_scores[2] + case3_scores[3]) / (float)reps_required;
                break;
        }

        return result;
    }

    void LaunchGame(int rep_id)
    {
        sceneScript.LaunchMiniGame(case_id, rep_id);
        inMiniGame = true;
    }

    float AddRepairResult()
    {
        float score = 0f;

        switch(case_id)
        {
            case 1: // CAR
                
                switch (rep_current_id)
                {
                    case 1:
                        score = buttonMasherGame.Score();
                        case1_scores[0] = score;
                        if (score > 0.5f)
                            Destroy(GameObject.Find("auto_ikkunarikki"));
                        break;

                    case 2:

                        break;

                    case 3:
                        score = spedeGame.Score();
                        case1_scores[2] = score;
                        if (score > 0.5f)
                            Destroy(GameObject.Find("auto_perakontti"));
                        break;

                    case 4:
                        score = spinGame.Score();
                        case1_scores[3] = score;
                        if (score > 0.5f)
                            Destroy(GameObject.Find("auto_rengaspuhki"));
                        break;
                }

                break;

            case 2: // JAR

                switch (rep_current_id)
                {
                    case 1:
                        score = buttonMasherGame.Score();
                        case2_scores[0] = score;
                        if (score > 0.5f)
                        {
                            Destroy(GameObject.Find("ruukku_kukka_dead"));
                            kukkaAlive.SetActive(true);
                        }
                        break;

                    case 2:
                        score = spinGame.Score();
                        case2_scores[1] = score;
                        if (score > 0.5f)
                            Destroy(GameObject.Find("ruukku_reika"));
                        break;

                    case 3:
                        score = spedeGame.Score();
                        case2_scores[2] = score;
                        if (score > 0.5f)
                            Destroy(GameObject.Find("ruukku_halkeamat"));
                        break;

                    case 4:

                        break;
                }

                break;

            case 3: // HUMAN

                switch (rep_current_id)
                {
                    case 1:
                        break;

                    case 2:
                        score = spinGame.Score();
                        case3_scores[1] = score;
                        if (score > 0.5f)
                        {
                            Destroy(GameObject.Find("heppu_housut_rikki"));
                            heppuHousuNeutral.SetActive(true);
                        }
                        if (score >= 1f)
                            heppuHousuBest.SetActive(true);
                        break;

                    case 3:
                        score = buttonMasherGame.Score();
                        case3_scores[2] = score;
                        if (score > 0.5f)
                        {
                            Destroy(GameObject.Find("heppu_tukka_rikki"));
                            heppuTukkaNeutral.SetActive(true);
                        }
                        if (score >= 1f)
                        {
                            heppuTukkaNeutral.SetActive(false);
                            heppuTukkaBest.SetActive(true);
                        }
                        break;

                    case 4:
                        score = spedeGame.Score();
                        case3_scores[0] = score;
                        if (score > 0.5f)
                        {
                            Destroy(GameObject.Find("heppu_torso_rikki"));
                            heppuTorsoNeutral.SetActive(true);
                        }
                        if (score >= 1f)
                            heppuTorsoBest.SetActive(true); 
                        break;
                }

                break;

            default:
                break;
        }
        return score;
    }
}
